require("dotenv").config();

const host = process.env.DEV === "true" ? "127.0.0.1" : "0.0.0.0";

// Require the framework and instantiate it
const fastify = require("fastify")({
  logger: true
});

// Declare a route
fastify.get("/", function(request, reply) {
  reply.send({ hello: "world" });
});

// Run the server!
fastify.listen(process.env.PORT, host, function(err, address) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
  fastify.log.info(`server listening on ${address}`);
});
